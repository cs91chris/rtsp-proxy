from distutils.core import setup
from distutils.extension import Extension


setup(
    name='RTSPProxy',
    ext_modules=[
        Extension(
            'rtspproxy',
            sources=[
                'src/wrapper_python.cpp',
                'src/rtsp_proxy.cpp'
            ],
            libraries=[
                'boost_python',
                'liveMedia',
                'groupsock',
                'UsageEnvironment',
                'BasicUsageEnvironment'
            ],
            language='c++'
        ),
    ]
)

