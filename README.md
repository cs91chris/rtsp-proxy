# RTSPProxyServer

Well tested with python 2.7 on debian 9.x


## live555 installation

This package depends on live555 libraries version 2018.08.28 so install its first:

1. download the source code from: https://github.com/cs91chris/live555/archive/2018.08.28.zip

2. extract the zip file and cd into it:
<pre>
    unzip live555-2018.08.28.zip
    cd live555-2018.08.28
</pre>

3. compile and install libraries as follow:
<pre>
    ./genMakefiles linux-with-shared-libraries
    make -j$(nproc)
    make install
</pre>

4. In order to fix a bug relative to header inclusion you have to create the following symlinks:
<pre>
    ln -s /usr/local/include/BasicUsageEnvironment/* /usr/local/include/live555/
    ln -s /usr/local/include/UsageEnvironment/* /usr/local/include/live555/
    ln -s /usr/local/include/groupsock/* /usr/local/include/live555/
    ln -s /usr/local/include/liveMedia/* /usr/local/include/live555/
</pre>

## RTSPProxy package installation

Now cd into folder that you have cloned this repo and run:
<pre>
    virtualenv env
    python setup.py install
</pre>
