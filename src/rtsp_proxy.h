#include <string>
#include <thread>

#include <live555/liveMedia.hh>
#include <live555/BasicUsageEnvironment.hh>

#define SERVER_DEFAULT_PORT         554
#define SERVER_ALTERNATIVE_PORT     8554
#define OUT_PACKET_BUFFER_SIZE      2000000

using namespace std;


class RTSPProxy
{
	public:
	    RTSPProxy();

	    void enableTCP(bool t);
	    void setVerbosityLevel(int v);
	    void setServerPort(int p);

	    void setAuthDB(string u, string p);
	    void setAuthRegister(string u, string p);

	    void start();

	    void addProxy(string p, string e);
	    void delProxy(string e);

	private:
	    void loop();

	    RTSPServer* server;
	    UsageEnvironment* env;
	    TaskScheduler* scheduler;

	    UserAuthenticationDatabase* authDB;
	    UserAuthenticationDatabase* authDBForREGISTER;

	    char* usernameREG;
	    char* passwordREG;
	    int verbosityLevel;

	    Boolean streamRTPOverTCP;
	    portNumBits tunnelOverHTTPPortNum;
	    portNumBits rtspServerPortNum;
};

