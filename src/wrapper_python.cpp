#include <boost/python.hpp>

#include "rtsp_proxy.h"


using namespace boost::python;


BOOST_PYTHON_MODULE(rtspproxy)
{
    class_<RTSPProxy> rtspProxy("RTSPProxy", init<>());

    rtspProxy.def( "enableTCP",         &RTSPProxy::enableTCP         );
    rtspProxy.def( "setVerbosityLevel", &RTSPProxy::setVerbosityLevel );
    rtspProxy.def( "setServerPort",     &RTSPProxy::setServerPort     );
    rtspProxy.def( "addProxy",          &RTSPProxy::addProxy          );
    rtspProxy.def( "delProxy",          &RTSPProxy::delProxy          );
    rtspProxy.def( "start",             &RTSPProxy::start             );
    rtspProxy.def( "setAuthDB",         &RTSPProxy::setAuthDB         );
    rtspProxy.def( "setAuthRegister",   &RTSPProxy::setAuthRegister   );

    scope().attr("RTSP_DEFAULT_PORT") = SERVER_DEFAULT_PORT;
    scope().attr("RTSP_ALTERNATIVE_PORT") = SERVER_ALTERNATIVE_PORT;
}

