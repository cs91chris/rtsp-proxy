#include "rtsp_proxy.h"


RTSPProxy::RTSPProxy()
{
    OutPacketBuffer::maxSize = OUT_PACKET_BUFFER_SIZE;

    verbosityLevel = 0;
    streamRTPOverTCP = False;
    tunnelOverHTTPPortNum = 0;
    rtspServerPortNum = SERVER_DEFAULT_PORT;

    server = NULL;
    scheduler = BasicTaskScheduler::createNew();
    env = BasicUsageEnvironment::createNew(*scheduler);

    authDB = NULL;

    usernameREG = NULL;
    passwordREG = NULL;
    authDBForREGISTER = NULL;
}

void RTSPProxy::loop()
{
    env->taskScheduler().doEventLoop();
}

void RTSPProxy::enableTCP(bool t)
{
    streamRTPOverTCP = t;

    if(t == True)
    {
        tunnelOverHTTPPortNum = (portNumBits)(~0);
    }
}

void RTSPProxy::setVerbosityLevel(int v)
{
    verbosityLevel = v;
}

void RTSPProxy::setServerPort(int p)
{
    rtspServerPortNum = p;
}

void RTSPProxy::setAuthRegister(string u, string p)
{
    usernameREG = strdup(u.c_str());
    passwordREG = strdup(p.c_str());

    if(authDBForREGISTER == NULL)
    {
        authDBForREGISTER = new UserAuthenticationDatabase;
    }
    authDBForREGISTER->addUserRecord(usernameREG, passwordREG);
}

void RTSPProxy::setAuthDB(string u, string p)
{
    if(authDB == NULL)
    {
        authDB = new UserAuthenticationDatabase;
    }
    authDB->addUserRecord(strdup(u.c_str()), strdup(p.c_str()));
}

void RTSPProxy::start()
{
    if(authDBForREGISTER != NULL)
    {
        server = RTSPServerWithREGISTERProxying::createNew(*env,
            rtspServerPortNum, authDB, authDBForREGISTER, 65,
            tunnelOverHTTPPortNum, verbosityLevel,
            usernameREG, passwordREG
        );
    }
    else
    {
        server = RTSPServer::createNew(*env,
            rtspServerPortNum, authDB
        );
    }
    if(server == NULL)
    {
        *env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
        exit(1);
    }

    std::thread t(&RTSPProxy::loop, this);
    t.detach();
}

void RTSPProxy::addProxy(string proxiedURL, string endpoint)
{
    ServerMediaSession* sms = ProxyServerMediaSession::createNew(
        *env, server,
        proxiedURL.c_str(), endpoint.c_str(),
	usernameREG, passwordREG,
        tunnelOverHTTPPortNum, verbosityLevel,
        -1, NULL, 0
    );

    server->addServerMediaSession(sms);

    *env << "RTSP stream, proxying the stream \"" << proxiedURL.c_str() << "\"\n";
    *env << "\tPlay this stream using the URL: " << server->rtspURL(sms) << "\n";
}

void RTSPProxy::delProxy(string e)
{
    server->removeServerMediaSession(e.c_str());
}
